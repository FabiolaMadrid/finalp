<?php

use Slim\Slim;

require_once "vendor/autoload.php";

    $app = new Slim();

    $app->get("/hola/:nombre", function($nombre) {
        echo "Hola ".$nombre;
    });

    $app->run();